package com.ruoyi.account.mapper;

import java.util.List;
import com.ruoyi.account.domain.AccRiskControl;

/**
 * 风控账户Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-11
 */
public interface AccRiskControlMapper 
{
    /**
     * 查询风控账户
     * 
     * @param id 风控账户主键
     * @return 风控账户
     */
    public AccRiskControl selectAccRiskControlById(Long id);

    /**
     * 查询风控账户列表
     * 
     * @param accRiskControl 风控账户
     * @return 风控账户集合
     */
    public List<AccRiskControl> selectAccRiskControlList(AccRiskControl accRiskControl);

    /**
     * 新增风控账户
     * 
     * @param accRiskControl 风控账户
     * @return 结果
     */
    public int insertAccRiskControl(AccRiskControl accRiskControl);

    /**
     * 修改风控账户
     * 
     * @param accRiskControl 风控账户
     * @return 结果
     */
    public int updateAccRiskControl(AccRiskControl accRiskControl);

    /**
     * 删除风控账户
     * 
     * @param id 风控账户主键
     * @return 结果
     */
    public int deleteAccRiskControlById(Long id);

    /**
     * 批量删除风控账户
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAccRiskControlByIds(Long[] ids);

    AccRiskControl selectByAdvertisersId(Long advertisersId);
}
