package com.ruoyi.account.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.account.mapper.AccRiskControlMapper;
import com.ruoyi.account.domain.AccRiskControl;
import com.ruoyi.account.service.IAccRiskControlService;

/**
 * 风控账户Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-11
 */
@Service
public class AccRiskControlServiceImpl implements IAccRiskControlService 
{
    @Autowired
    private AccRiskControlMapper accRiskControlMapper;

    /**
     * 查询风控账户
     * 
     * @param id 风控账户主键
     * @return 风控账户
     */
    @Override
    public AccRiskControl selectAccRiskControlById(Long id)
    {
        return accRiskControlMapper.selectAccRiskControlById(id);
    }

    /**
     * 查询风控账户列表
     * 
     * @param accRiskControl 风控账户
     * @return 风控账户
     */
    @Override
    public List<AccRiskControl> selectAccRiskControlList(AccRiskControl accRiskControl)
    {
        return accRiskControlMapper.selectAccRiskControlList(accRiskControl);
    }

    /**
     * 新增风控账户
     * 
     * @param accRiskControl 风控账户
     * @return 结果
     */
    @Override
    public int insertAccRiskControl(AccRiskControl accRiskControl)
    {
        return accRiskControlMapper.insertAccRiskControl(accRiskControl);
    }

    /**
     * 修改风控账户
     * 
     * @param accRiskControl 风控账户
     * @return 结果
     */
    @Override
    public int updateAccRiskControl(AccRiskControl accRiskControl)
    {
        return accRiskControlMapper.updateAccRiskControl(accRiskControl);
    }

    /**
     * 批量删除风控账户
     * 
     * @param ids 需要删除的风控账户主键
     * @return 结果
     */
    @Override
    public int deleteAccRiskControlByIds(Long[] ids)
    {
        return accRiskControlMapper.deleteAccRiskControlByIds(ids);
    }

    /**
     * 删除风控账户信息
     * 
     * @param id 风控账户主键
     * @return 结果
     */
    @Override
    public int deleteAccRiskControlById(Long id)
    {
        return accRiskControlMapper.deleteAccRiskControlById(id);
    }
}
