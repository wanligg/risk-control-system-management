package com.ruoyi.account.service.impl;

import java.util.List;

import com.ruoyi.account.domain.AccRiskControl;
import com.ruoyi.account.mapper.AccRiskControlMapper;
import com.ruoyi.account.service.IAccRiskControlService;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.exception.GlobalException;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.account.mapper.AccUserMapper;
import com.ruoyi.account.domain.AccUser;
import com.ruoyi.account.service.IAccUserService;

/**
 * 账户列表Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-11
 */
@Service
public class AccUserServiceImpl implements IAccUserService 
{
    @Autowired
    private AccUserMapper accUserMapper;
    @Autowired
    private AccRiskControlMapper accRiskControlMapper;

    /**
     * 查询账户列表
     * 
     * @param id 账户列表主键
     * @return 账户列表
     */
    @Override
    public AccUser selectAccUserById(Long id)
    {
        return accUserMapper.selectAccUserById(id);
    }

    /**
     * 查询账户列表列表
     * 
     * @param accUser 账户列表
     * @return 账户列表
     */
    @Override
    public List<AccUser> selectAccUserList(AccUser accUser)
    {
        return accUserMapper.selectAccUserList(accUser);
    }

    /**
     * 新增账户列表
     * 
     * @param accUser 账户列表
     * @return 结果
     */
    @Override
    public int insertAccUser(AccUser accUser)
    {
        accUser.setCreateTime(DateUtils.getNowDate());
        return accUserMapper.insertAccUser(accUser);
    }

    /**
     * 修改账户列表
     * 
     * @param accUser 账户列表
     * @return 结果
     */
    @Override
    public int updateAccUser(AccUser accUser)
    {
        return accUserMapper.updateAccUser(accUser);
    }

    /**
     * 批量删除账户列表
     * 
     * @param ids 需要删除的账户列表主键
     * @return 结果
     */
    @Override
    public int deleteAccUserByIds(Long[] ids)
    {
        return accUserMapper.deleteAccUserByIds(ids);
    }

    /**
     * 删除账户列表信息
     * 
     * @param id 账户列表主键
     * @return 结果
     */
    @Override
    public int deleteAccUserById(Long id)
    {
        return accUserMapper.deleteAccUserById(id);
    }


    /**
     * 移入风控
     * @param accUser
     * @return
     */
    @Override
    public int riskControl(AccUser accUser) {
        //判断该用户是否已经存在风控中
        AccRiskControl riskControl = accRiskControlMapper.selectByAdvertisersId(accUser.getAdvertisersId());
        if (riskControl != null){
            throw new GlobalException("该用户已经存在风控中!");
        }
        // 存入风控表
        riskControl = new AccRiskControl();
        riskControl.setAdvertisersId(accUser.getAdvertisersId());
        riskControl.setAgentId(accUser.getAdvertisersId());
        return accRiskControlMapper.insertAccRiskControl(riskControl);
    }
}
