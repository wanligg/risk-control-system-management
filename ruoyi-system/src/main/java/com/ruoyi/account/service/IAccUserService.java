package com.ruoyi.account.service;

import java.util.List;
import com.ruoyi.account.domain.AccUser;

/**
 * 账户列表Service接口
 * 
 * @author ruoyi
 * @date 2023-04-11
 */
public interface IAccUserService 
{
    /**
     * 查询账户列表
     * 
     * @param id 账户列表主键
     * @return 账户列表
     */
    public AccUser selectAccUserById(Long id);

    /**
     * 查询账户列表列表
     * 
     * @param accUser 账户列表
     * @return 账户列表集合
     */
    public List<AccUser> selectAccUserList(AccUser accUser);

    /**
     * 新增账户列表
     * 
     * @param accUser 账户列表
     * @return 结果
     */
    public int insertAccUser(AccUser accUser);

    /**
     * 修改账户列表
     * 
     * @param accUser 账户列表
     * @return 结果
     */
    public int updateAccUser(AccUser accUser);

    /**
     * 批量删除账户列表
     * 
     * @param ids 需要删除的账户列表主键集合
     * @return 结果
     */
    public int deleteAccUserByIds(Long[] ids);

    /**
     * 删除账户列表信息
     * 
     * @param id 账户列表主键
     * @return 结果
     */
    public int deleteAccUserById(Long id);

    /**
     * 移入风控
     * @param accUser
     * @return
     */
    int riskControl(AccUser accUser);
}
