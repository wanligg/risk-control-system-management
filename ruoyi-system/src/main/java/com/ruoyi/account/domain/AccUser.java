package com.ruoyi.account.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 账户列表对象 acc_user
 * 
 * @author ruoyi
 * @date 2023-04-11
 */
@Data
public class AccUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 广告主ID */
    @Excel(name = "广告主ID")
    private Long advertisersId;

    /** 账户名 */
    @Excel(name = "账户名")
    private String name;

    /** 公司名 */
    @Excel(name = "公司名")
    private String company;

    /** 经营类别 */
    @Excel(name = "经营类别")
    private String brand;

    /** 运营省份 */
    @Excel(name = "运营省份")
    private String promotionCenterProvince;

    /** 运营城市 */
    @Excel(name = "运营城市")
    private String promotionCenterCity;

    /** 运营区域 */
    @Excel(name = "运营区域")
    private String promotionArea;

    /** 执照省份 */
    @Excel(name = "执照省份")
    private String licenseProvince;

    /** 一级行业名称 */
    @Excel(name = "一级行业名称")
    private String firstIndustryName;

    /** 二级行业名称 */
    @Excel(name = "二级行业名称")
    private String secondIndustryName;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 行业 */
    @Excel(name = "行业")
    private String industry;

}
