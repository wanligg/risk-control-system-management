package com.ruoyi.account.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 风控账户对象 acc_risk_control
 * 
 * @author ruoyi
 * @date 2023-04-11
 */
public class AccRiskControl extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 广告主ID */
    @Excel(name = "广告主ID")
    private Long advertisersId;

    /** 代理商ID */
    @Excel(name = "代理商ID")
    private Long agentId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setAdvertisersId(Long advertisersId) 
    {
        this.advertisersId = advertisersId;
    }

    public Long getAdvertisersId() 
    {
        return advertisersId;
    }
    public void setAgentId(Long agentId) 
    {
        this.agentId = agentId;
    }

    public Long getAgentId() 
    {
        return agentId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("advertisersId", getAdvertisersId())
            .append("agentId", getAgentId())
            .toString();
    }
}
