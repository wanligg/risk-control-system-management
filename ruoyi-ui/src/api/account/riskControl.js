import request from '@/utils/request'

// 查询风控账户列表
export function listRiskControl(query) {
  return request({
    url: '/account/riskControl/list',
    method: 'get',
    params: query
  })
}

// 查询风控账户详细
export function getRiskControl(id) {
  return request({
    url: '/account/riskControl/' + id,
    method: 'get'
  })
}

// 新增风控账户
export function addRiskControl(data) {
  return request({
    url: '/account/riskControl',
    method: 'post',
    data: data
  })
}

// 修改风控账户
export function updateRiskControl(data) {
  return request({
    url: '/account/riskControl',
    method: 'put',
    data: data
  })
}

// 删除风控账户
export function delRiskControl(id) {
  return request({
    url: '/account/riskControl/' + id,
    method: 'delete'
  })
}
