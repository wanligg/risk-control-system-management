import request from '@/utils/request'


// 移入风控
export function riskControl(data) {
  return request({
    url: '/account/user/riskControl',
    method: 'post',
    data: data
  })
}

// 查询账户列表列表
export function listUser(query) {
  return request({
    url: '/account/user/list',
    method: 'get',
    params: query
  })
}

// 查询账户列表详细
export function getUser(id) {
  return request({
    url: '/account/user/' + id,
    method: 'get'
  })
}

// 新增账户列表
export function addUser(data) {
  return request({
    url: '/account/user',
    method: 'post',
    data: data
  })
}

// 修改账户列表
export function updateUser(data) {
  return request({
    url: '/account/user',
    method: 'put',
    data: data
  })
}

// 删除账户列表
export function delUser(id) {
  return request({
    url: '/account/user/' + id,
    method: 'delete'
  })
}
