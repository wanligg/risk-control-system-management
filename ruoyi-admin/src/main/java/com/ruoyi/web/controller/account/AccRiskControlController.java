package com.ruoyi.web.controller.account;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.account.domain.AccRiskControl;
import com.ruoyi.account.service.IAccRiskControlService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 风控账户Controller
 * 
 * @author ruoyi
 * @date 2023-04-11
 */
@RestController
@RequestMapping("/account/riskControl")
public class AccRiskControlController extends BaseController
{
    @Autowired
    private IAccRiskControlService accRiskControlService;

    /**
     * 查询风控账户列表
     */
    @PreAuthorize("@ss.hasPermi('account:riskControl:list')")
    @GetMapping("/list")
    public TableDataInfo list(AccRiskControl accRiskControl)
    {
        startPage();
        List<AccRiskControl> list = accRiskControlService.selectAccRiskControlList(accRiskControl);
        return getDataTable(list);
    }

    /**
     * 导出风控账户列表
     */
    @PreAuthorize("@ss.hasPermi('account:riskControl:export')")
    @Log(title = "风控账户", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AccRiskControl accRiskControl)
    {
        List<AccRiskControl> list = accRiskControlService.selectAccRiskControlList(accRiskControl);
        ExcelUtil<AccRiskControl> util = new ExcelUtil<AccRiskControl>(AccRiskControl.class);
        util.exportExcel(response, list, "风控账户数据");
    }

    /**
     * 获取风控账户详细信息
     */
    @PreAuthorize("@ss.hasPermi('account:riskControl:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(accRiskControlService.selectAccRiskControlById(id));
    }

    /**
     * 新增风控账户
     */
    @PreAuthorize("@ss.hasPermi('account:riskControl:add')")
    @Log(title = "风控账户", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AccRiskControl accRiskControl)
    {
        return toAjax(accRiskControlService.insertAccRiskControl(accRiskControl));
    }

    /**
     * 修改风控账户
     */
    @PreAuthorize("@ss.hasPermi('account:riskControl:edit')")
    @Log(title = "风控账户", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AccRiskControl accRiskControl)
    {
        return toAjax(accRiskControlService.updateAccRiskControl(accRiskControl));
    }

    /**
     * 删除风控账户
     */
    @PreAuthorize("@ss.hasPermi('account:riskControl:remove')")
    @Log(title = "风控账户", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(accRiskControlService.deleteAccRiskControlByIds(ids));
    }
}
