package com.ruoyi.web.controller.account;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.account.domain.AccUser;
import com.ruoyi.account.service.IAccUserService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 账户列表Controller
 * 
 * @author ruoyi
 * @date 2023-04-11
 */
@RestController
@RequestMapping("/account/user")
public class AccUserController extends BaseController
{
    @Autowired
    private IAccUserService accUserService;

    /**
     * 查询账户列表列表
     */
    @PreAuthorize("@ss.hasPermi('account:user:list')")
    @GetMapping("/list")
    public TableDataInfo list(AccUser accUser)
    {
        startPage();
        List<AccUser> list = accUserService.selectAccUserList(accUser);
        return getDataTable(list);
    }

    /**
     * 导出账户列表列表
     */
    @PreAuthorize("@ss.hasPermi('account:user:export')")
    @Log(title = "账户列表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AccUser accUser)
    {
        List<AccUser> list = accUserService.selectAccUserList(accUser);
        ExcelUtil<AccUser> util = new ExcelUtil<AccUser>(AccUser.class);
        util.exportExcel(response, list, "账户列表数据");
    }

    /**
     * 获取账户列表详细信息
     */
    @PreAuthorize("@ss.hasPermi('account:user:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(accUserService.selectAccUserById(id));
    }

    /**
     * 新增账户列表
     */
    @PreAuthorize("@ss.hasPermi('account:user:add')")
    @Log(title = "账户列表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AccUser accUser)
    {
        return toAjax(accUserService.insertAccUser(accUser));
    }

    /**
     * 修改账户列表
     */
    @PreAuthorize("@ss.hasPermi('account:user:edit')")
    @Log(title = "账户列表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AccUser accUser)
    {
        return toAjax(accUserService.updateAccUser(accUser));
    }

    /**
     * 删除账户列表
     */
    @PreAuthorize("@ss.hasPermi('account:user:remove')")
    @Log(title = "账户列表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(accUserService.deleteAccUserByIds(ids));
    }

    /**
     * 添加风控
     */
    @PreAuthorize("@ss.hasPermi('account:user:riskControl')")
    @Log(title = "账户列表", businessType = BusinessType.INSERT)
    @PostMapping("/riskControl")
    public AjaxResult riskControl(@RequestBody AccUser accUser)
    {
        return toAjax(accUserService.riskControl(accUser));
    }


    /**
     * 账号授权
     */
    @PreAuthorize("@ss.hasPermi('account:user:authorization')")
    @Log(title = "账户列表", businessType = BusinessType.OTHER)
    @PostMapping("/authorization")
    public AjaxResult authorization(@RequestBody AccUser accUser)
    {
        return toAjax(accUserService.riskControl(accUser));
    }
}
